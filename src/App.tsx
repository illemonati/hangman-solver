import React from 'react';
import './App.css';
import Solver from './components/solver/solver';

const App: React.FC = () => {
  return (
    <div className="App">
      <Solver />
    </div>
  );
}

export default App;
