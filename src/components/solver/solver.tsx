import React, { useEffect, useState } from 'react';
import './solver.css'


export default function Solver () {

    const [dictionary, setDictionary] = useState(['']);
    const [word, setWord] = useState('');
    const [excluded, setExcluded] = useState('');
    const [suggestions, setSuggestions] = useState(<div id='suggestions'></div>);


    const getDictionary = async (dictionaryPath: string) => {
        const res = await fetch(dictionaryPath);
        const dic = await res.text();
        const lines = dic.split('\r\n');
        // console.log(lines);
        let l2 = [];
        lines.forEach((l) => {
            l2.push(l.trim())
        })
        setDictionary(lines);
        // console.log('done');
    }


    const updateSuggestions = () => {
        const wordlen = word.length;
        const indexesNotFilled = locations(word, '.');
        let dict : string[] = [];
        dictionary.forEach((w: string) => {
            if (w.length === wordlen) {
                let ok = true;
                excluded.split('').forEach((l) => {
                    if (w.indexOf(l) !== -1) {
                        ok = false;
                        return;
                    }
                });
                if (ok) {
                    dict.push(w);
                }
            }
        })
        // console.log(dict)

        let dict2 : string[] = [];
        dict.forEach((w: string) => {
            for (let i = 0; i < wordlen; i++) {
                if (indexesNotFilled.indexOf(i) === -1) {
                    if (w[i] !== word[i]) {
                        return;
                    }
                }
            }
            dict2.push(w);
        })


        let dict3 : string[] = [];
        dict2.forEach((w: string) => {
            let ok = true;
            indexesNotFilled.forEach((i) => {
                word.split('').forEach((l)=> {
                    if (l === w[i]) {
                        ok = false;
                        return;
                    }
                });
            });
            if (ok) {
                dict3.push(w);
            }
        });

        // console.log(dict2);
        let count: {[key: string]: number;} = {};
        count = {
            'a' : 0,
            'b' : 0,
            'c' : 0,
            'd' : 0,
            'e' : 0,
            'f' : 0,
            'g' : 0,
            'h' : 0,
            'i' : 0,
            'j' : 0,
            'k' : 0,
            'l' : 0,
            'm' : 0,
            'n' : 0,
            'o' : 0,
            'p' : 0,
            'q' : 0,
            'r' : 0,
            's' : 0,
            't' : 0,
            'u' : 0,
            'v' : 0,
            'w' : 0,
            'x' : 0,
            'y' : 0,
            'z' : 0
        }
        let percent = count;
        dict3.forEach((w: string) => {
            let ls: Set<string> = new Set();
            indexesNotFilled.forEach((i) => {
                const letter = w[i];
                ls.add(letter);
            });
            ls.forEach((l) => {
                count[l] ++;
            });
            // console.log(count);
        });
        // console.log(count);

        if (dict3.length > 0) {
            Object.keys(percent).forEach((l) => {
                percent[l] = count[l]/dict3.length;
            });
        }
        var psorted = Object.keys(percent).map((l) => {
            return [l, percent[l]];
          });
          
        psorted.sort(function(first, second) {
            return second[1] as number - (first[1] as number);
        });
        // console.log(psorted);

        let confirmedWord = null;
        if (dict3.length === 1 && dict3[0] !== '') {
            confirmedWord = (
                <h2>Confirmed Word: {dict3[0]}</h2>
            );
        }



        setSuggestions(
            <div id='suggestions'>
                {confirmedWord}
                <h2>Suggestions</h2>
                {Object.keys(psorted).map((_, i: number) => {
                    return (<p> {psorted[i][0]} : {(psorted[i][1]) as number *100}% </p>);
                })}
            </div>
        );

        



    };







    const handleWordUpdate = (e: React.ChangeEvent<HTMLInputElement>) => {
        let val = e.currentTarget.value;
        val = val.toLowerCase();
        setWord(val);
    };

    const handleExcludedUpdate = (e: React.ChangeEvent<HTMLInputElement>) => {
        let val = e.currentTarget.value;
        val = Array.from(new Set(val.split(''))).join('');
        val = val.toLowerCase();
        setExcluded(val);
    }

    const locations = (input: string, sub: string) => {
        var a=[],i=-1;
        while((i=input.indexOf(sub,i+1)) >= 0) a.push(i);
        return a;
    }
      



    useEffect(() => {
        updateSuggestions();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [word, excluded]);


    useEffect(() => {
        getDictionary('words_alpha.txt').then();
    }, []);



    return (
        <div className="Solver">
        <h1>Hangman Solver</h1>

            <div id='not-loaded' style={{'display': dictionary.length > 1 ? 'none' : 'block'}}>
                <p>Now Loading the Dictionary</p>
            </div>
            <div id='loaded' style={{'display': dictionary.length > 1 ? 'block' : 'none'}}>
                <p>Dictionary Loaded!</p>
                <p>Words: {dictionary.length}</p>
                <br />
                <p>Word: (put . for missing letters)</p>
                <input className='centered' type='text' id='word' onChange={(e) => handleWordUpdate(e)} autoComplete='off' autoCorrect='off' autoCapitalize='off' spellCheck={false} ></input>
                <br />
                <p>Excluded letters</p>
                <input className='centered' type='text' id='exclude' onChange={(e) => handleExcludedUpdate(e)} value={excluded} autoComplete='off' autoCorrect='off' autoCapitalize='off' spellCheck={false} ></input>
                <br />
                <br />
                {suggestions}
            </div>
        </div>
    );
}


